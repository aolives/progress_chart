import os
import time
import yaml
from watchdog.observers.polling import PollingObserver as Observer
from watchdog.events import PatternMatchingEventHandler
from progress_chart.reports.report_read import check_and_import


def build_event_handler():
    patterns = "*"
    ignore_patterns = ""
    ignore_directories = False
    case_sensitive = True
    return PatternMatchingEventHandler(
        patterns, ignore_patterns, ignore_directories, case_sensitive
    )


def build_observer(event_handler):
    my_path = get_watch_path()
    go_recursively = False
    observer = Observer()
    observer.schedule(event_handler, my_path, recursive=go_recursively)
    return observer


def get_watch_path():
    path = "."
    if os.name == "nt":
        try:
            with open(r'.\private\settings.yaml') as file:
                path = yaml.full_load(file)["path"]
        except IOError as e:
            print(
                """
                Have you set a path in ./private/settings.yaml? I doubt it...\n
                Monitoring current directory instead.
                """
            )
    elif os.name == "posix":
        try:
            path = "/mnt/pdq-reports"
        except OSError as e:
            print("Ugh")
    print(path)
    return path


def run():
    my_event_handler = build_event_handler()
    my_event_handler.on_created = check_and_import
    my_observer = build_observer(my_event_handler)
    my_observer.start()
    print("(o_o) Ready any time!")

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
