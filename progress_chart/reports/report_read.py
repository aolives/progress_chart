import csv
import re
import sqlite3
import time
from datetime import datetime
from os.path import split as path_split


def check_and_import(event):
    time.sleep(1)
    if file_is_good(event.src_path):
        process_csv(event.src_path)


def file_is_good(path):
    file_name = path_split(path)[1]
    if file_name == "TESTCSV-2_12_2020 8_55_28 PM.csv":
        return True
    if file_name.startswith("Application Versions Count") and file_name.endswith("csv"):
        return True


def process_csv(path):
    a_datetime = get_datetime_from_path(path)
    conn = sqlite3.connect("./private/test.db")
    print("(^_^) Connected to SQLite!")
    with open(path, newline="", encoding="ISO-8859-1") as report:
        reader = csv.reader(report, delimiter=",", quotechar='"')
        next(reader)
        for row in reader:
            insert_row(row, a_datetime, conn)
    conn.close()
    print("(-_-) Disconnected from SQLite!")


def get_datetime_from_path(path):
    file_name = path_split(path)[1]
    datetime_str = re.search(
        r"([1-9]|10|11|12)_([1-9]|1[0-9]|2[0-9]|3[0-1]|)_\d{4} ([1-9]|10|11|12)_[0-5]\d_[0-5]\d (A|P)M",
        file_name,
    ).group(0)
    datetime_obj = datetime.strptime(datetime_str, "%m_%d_%Y %I_%M_%S %p")
    # print(datetime_str, datetime_obj, type(datetime_obj))
    return datetime_obj


def insert_row(row, datetime_obj, conn):
    """Reads a single row and inserts it into the database.

    Probably needs to be completely reworked.
    My thought on the rework is that these should be three different methods
    also instead of attempting to add every column it could try to add from
    columns most often added and work backwards somehow.
    """

    # Application
    try:
        with conn:
            conn.execute("INSERT INTO applications VALUES (?,?)", (None, row[0]))
    except sqlite3.IntegrityError as e:
        pass
        # print("\nApp is already in there bud...")
    c = conn.cursor()
    c.execute("SELECT * FROM applications WHERE name=?", (row[0],))
    application_id = c.fetchone()[0]
    # print("a", application_id)

    # Version
    version_w_nulls = re.findall(r"\d+", row[1])
    version = []
    for i in range(4):
        try:
            version.append(version_w_nulls[i])
        except:
            version.append(0)
    try:
        with conn:
            conn.execute(
                "INSERT INTO versions VALUES (?,?,?,?,?,?)",
                (None, application_id, version[0], version[1], version[2], version[3]),
            )
    except sqlite3.IntegrityError as e:
        pass
        # print("\nVer is already in there bud...")
    c.execute(
        "SELECT * FROM versions WHERE application_id=? AND version_maj=? AND version_min=? AND version_rev=? AND version_bui=?",
        (application_id, version[0], version[1], version[2], version[3]),
    )
    version_id = c.fetchone()[0]
    c.close()
    # print("v", version_id)

    # Counts over Time
    try:
        with conn:
            conn.execute(
                "INSERT INTO counts_over_time VALUES (?,?,?)",
                (version_id, datetime_obj, row[2]),
            )
    except sqlite3.IntegrityError as e:
        pass
        # print("\nCoT is already in there bud...")
