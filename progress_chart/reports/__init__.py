import sqlite3


conn = sqlite3.connect("./private/test.db")
conn.execute("PRAGMA foreign_keys = 1")

create_app_table_query = """CREATE TABLE applications (
                            id integer PRIMARY KEY,
                            name text NOT NULL UNIQUE);"""

create_ver_table_query = """CREATE TABLE versions (
                            id integer PRIMARY KEY,
                            application_id integer NOT NULL,
                            version_maj integer,
                            version_min integer,
                            version_rev integer,
                            version_bui integer,
                            UNIQUE(application_id, version_maj, version_min, version_rev, version_bui),
                            FOREIGN KEY(application_id) REFERENCES applications(id));"""

create_cot_table_query = """CREATE TABLE counts_over_time (
                            version_id integer NOT NULL,
                            date_time timestamp NOT NULL,
                            count integer NOT NULL,
                            UNIQUE(version_id, date_time),
                            FOREIGN KEY(version_id) REFERENCES versions(id));"""

try:
    with conn:
        conn.execute(create_app_table_query)
        conn.execute(create_ver_table_query)
        conn.execute(create_cot_table_query)
except sqlite3.OperationalError:
    pass
conn.close()
