import os
import yaml


try:
    with open(r".\private\settings.yaml") as file:
        pass
except IOError:
    try:
        os.mkdir(r".\private")
    except FileExistsError:
        pass
    with open(r".\private\settings.yaml", "w+") as file:
        file.write(
            """
            # Enter the path on the line directly below, for UNC use //machine/dir/dir format\n
            path: .\n
            \n
            applications:\n
              - name LIKE 'Google Chrome%'
            """
        )
