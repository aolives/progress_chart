import dash
import yaml
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from datetime import datetime
from dash.dependencies import Input, Output
from flask_caching import Cache
import plotly.graph_objects as go
import plotly.io as pio
import pandas as pd
import sqlite3


app = dash.Dash(__name__, external_stylesheets=[dbc.themes.DARKLY])
server = app.server
application = app
cache = Cache(
    app.server,
    config={"CACHE_TYPE": "filesystem", "CACHE_DIR": "./private/cache-directory"},
)
TIMEOUT = 60
theme = pio.templates["plotly_dark"]


@cache.memoize(timeout=TIMEOUT)
def query_data():
    conn = sqlite3.connect("./private/test.db")
    c = conn.cursor()
    data = []

    with open(r".\private\settings.yaml") as file:
        list = yaml.full_load(file)["applications"]
        for app in list:
            app_record = get_counts(c, app)
            x, y = [], []
            label = app_record[0][0]
            print(label)
            for count in app_record:
                data.append([label, count[1], count[2]])
                # x.append(count[1])
                # y.append(count[2])

    df = pd.DataFrame(data, columns=["Name", "Datetime", "Count"])
    print(df)
    return df


def get_counts(c, app):
    c.execute(
        """
            SELECT
                name,
                date_time,
                SUM(count)
            FROM counts_over_time
            JOIN versions ON counts_over_time.version_id = versions.id
            JOIN applications ON versions.application_id = applications.id
            WHERE %s AND (versions.id, date_time) NOT IN (
                SELECT version, date_time
                FROM (
                    SELECT
                        versions.id AS version,
                        date_time,
                        MAX((version_maj * 1000000000000000) + (version_min * 10000000000) + (version_rev * 100000) + version_bui),
                        version_maj,
                        version_min,
                        version_rev,
                        version_bui
                    FROM counts_over_time
                    JOIN versions ON counts_over_time.version_id = versions.id
                    JOIN applications ON versions.application_id = applications.id
                    WHERE %s
                    GROUP BY date_time
                )
                GROUP BY date_time
            )
            GROUP BY date_time
        """
        % (app, app)
    )
    return c.fetchall()


def serve_layout():
    return html.Div(
        [
            dbc.NavbarSimple(
                [dbc.Button("Refresh", id="refresh")],
                brand="Dashboard",
                color="dark",
                dark=True,
            ),
            # dcc.Dropdown(id='live-dropdown', value='1', options=[{'label': 'A', 'value': 1},{'label': 'B', 'value': 2}]),
            dbc.Card(
                [
                    dbc.CardHeader("Out of Date Application Counts"),
                    dbc.CardBody(
                        [dcc.Graph(id="live-graph")], style={"padding": "8px"}
                    ),
                ],
                style={"margin": "16px"},
            ),
        ]
    )


app.layout = serve_layout


@app.callback(Output("live-graph", "figure"), [Input("refresh", "n_clicks")])
def update_live_graph(n_clicks):
    df = query_data()
    return {
        "data": [
            dict(
                x=df[df["Name"] == i]["Datetime"],
                y=df[df["Name"] == i]["Count"],
                mode="line",
                name=i,
            )
            for i in df.Name.unique()
        ],
        "layout": {
            "xaxis": dict(
                # range = [,datetime.datetime.now()],
                rangeselector=dict(
                    buttons=list(
                        [
                            dict(count=1, label="1d", step="day", stepmode="todate"),
                            dict(count=7, label="1w", step="day", stepmode="todate"),
                            dict(count=1, label="1m", step="month", stepmode="todate"),
                            dict(count=1, label="1y", step="year", stepmode="todate"),
                            dict(step="all"),
                        ]
                    ),
                    bgcolor="#303030",
                ),
                rangeslider=dict(visible=True),
                type="date",
            ),
            "yaxis": dict(title="Install Counts", rangemode="tozero"),
            "template": theme,
            "height": 700,
            "paper_bgcolor": "#303030",
            "margin": dict(l=16, r=16, t=16, b=16),
        },
    }


def run():
    app.run_server(debug=True)
